﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp28
{
    public partial class Form1 : Form
    {
        private DataTable scheduleDataTable;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void завантажитиТаблиуцюЗФайлаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text Files (*.txt)|*.txt";
            openFileDialog.Title = "Виберіть таблицю розкладу";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedFilePath = openFileDialog.FileName;
                LoadStudentScheduleFromFile(selectedFilePath);
            }
        }
        private void LoadStudentScheduleFromFile(string filePath)
        {

            scheduleDataTable = new DataTable();
            scheduleDataTable.Columns.Add("Номер рейсу", typeof(string));
            scheduleDataTable.Columns.Add("Місто прибуття", typeof(string));
            scheduleDataTable.Columns.Add("Кількість місць", typeof(string));
            scheduleDataTable.Columns.Add("Час перельоту", typeof(string));
            scheduleDataTable.Columns.Add("Ціна білету", typeof(string));

            try
            {
                //Зчитуємо з файлу 
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] scheduleEntry = line.Split(',');
                        if (scheduleEntry.Length == 5)
                        {
                            scheduleDataTable.Rows.Add(scheduleEntry);
                        }
                    }
                }

                dataGridView1.DataSource = scheduleDataTable;
            }
            catch (IOException ex)
            {
                MessageBox.Show("Помилка при читанні файлу: " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        private int CountFlightsWithPriceGreaterThan(DataTable schedule, int amount)
        {
            int count = 0;
            foreach (DataRow row in schedule.Rows)
            {
                if (row["Ціна білету"] != null && row["Ціна білету"] != DBNull.Value)
                {
                    if (int.TryParse(row["Ціна білету"].ToString(), out int price))
                    {
                        if (price > amount)
                        {
                            count++;
                        }
                    }
                }
            }
            return count;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox1.Text, out int inputAmount))
            {
                int flightsCount = CountFlightsWithPriceGreaterThan(scheduleDataTable, inputAmount);
                MessageBox.Show("Кількість рейсів з ціною більшою ніж " + inputAmount + " грн: " + flightsCount);
            }
            else
            {
                MessageBox.Show("Введіть коректну кількість грошей.");
            }
        }
        private void Countofnumberplaces(DataTable schedule, string destination, int maxDuration, out int passengerCount, out int totalSeats)
        {
            passengerCount = 0;
            totalSeats = 0;
            foreach (DataRow row in schedule.Rows)
            {
                string flightDestination = row["Місто прибуття"].ToString();
                if (flightDestination.Equals(destination, StringComparison.OrdinalIgnoreCase))
                {
                    if (row["Час перельоту"] != null && row["Час перельоту"] != DBNull.Value)
                    {
                        if (int.TryParse(row["Час перельоту"].ToString(), out int duration))
                        {
                            if (duration <= maxDuration)
                            {
                                int seats = int.Parse(row["Кількість місць"].ToString());
                                totalSeats += seats;
                                passengerCount += seats;
                            }
                        }
                    }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string destination = textBox3.Text;
            if (!string.IsNullOrWhiteSpace(destination))
            {
                if (int.TryParse(textBox2.Text, out int maxDuration))
                {
                    Countofnumberplaces(scheduleDataTable, destination, maxDuration, out int passengerCount, out int totalSeats);
                    MessageBox.Show("Кількість пасажирів, які летять до міста " + destination + " з тривалістю перельоту не більше " + maxDuration + " год: " + passengerCount);
                    MessageBox.Show("Загальна кількість місць з тривалістю перельоту не більше " + maxDuration + " год: " + totalSeats);
                }
                else
                {
                    MessageBox.Show("Введіть коректну тривалість перельоту.");
                }
            }
            else
            {
                MessageBox.Show("Введіть місто прибуття.");
            }
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Зберігаємо датагрід в текстовий файл і якщо стається помилка то спрацьовує catch який містить повідомлення "Помилка при збереженні файлу"
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt";
            saveFileDialog.Title = "Зберегти таблицю";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedFilePath = saveFileDialog.FileName;

                try
                {
                    using (StreamWriter writer = new StreamWriter(selectedFilePath))
                    {
                        foreach (DataRow row in scheduleDataTable.Rows)
                        {
                            string scheduleEntry = string.Join(",", row.ItemArray);
                            writer.WriteLine(scheduleEntry);
                        }
                    }

                    MessageBox.Show("Таблиця була успішно збережений у файл.", "Інформація",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Помилка при збереженні файлу: " + ex.Message, "Помилка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void зберегтиВExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel Files (*.xlsx)|*.xlsx";
            saveFileDialog.Title = "Зберегти таблицю у форматі Excel";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedFilePath = saveFileDialog.FileName;

                try
                {
                    using (ExcelPackage excelPackage = new ExcelPackage())
                    {
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Розклад рейсів");

                        // Заповнюємо заголовки стовпців
                        for (int i = 0; i < scheduleDataTable.Columns.Count; i++)
                        {
                            worksheet.Cells[1, i + 1].Value = scheduleDataTable.Columns[i].ColumnName;
                        }

                        // Заповнюємо дані таблиці
                        for (int i = 0; i < scheduleDataTable.Rows.Count; i++)
                        {
                            for (int j = 0; j < scheduleDataTable.Columns.Count; j++)
                            {
                                worksheet.Cells[i + 2, j + 1].Value = scheduleDataTable.Rows[i][j];
                            }
                        }

                        // Зберігаємо Excel-файл
                        FileInfo file = new FileInfo(selectedFilePath);
                        excelPackage.SaveAs(file);
                    }

                    MessageBox.Show("Таблицю було успішно збережено у форматі Excel.", "Інформація",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Помилка при збереженні файлу: " + ex.Message, "Помилка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
